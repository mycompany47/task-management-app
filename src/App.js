import React from "react";
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import CreateTask from "./pages/Createtask";


const router = createBrowserRouter([
  {
    path: "/",
    element: <CreateTask></CreateTask>,
  }
]);


function App() {
  return (
    <div className="App">
        <RouterProvider router={router} />
    </div>
  );
}

export default App;
