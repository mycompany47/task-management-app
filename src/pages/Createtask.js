import React from 'react';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import * as XLSX from 'xlsx';
import { TfiMarkerAlt, TfiTrash, TfiExport } from "react-icons/tfi";
import Alert from 'react-bootstrap/Alert';



export default function CreateTask() {

    const [showDiv, setShowDiv] = React.useState(false);



    const [err, setErr] = useState(false);
    const [selectedTaskIndex, setselectedTaskIndex] = useState(null)
    const [changeValueOfEdit, setchangeValueOfEdit] = useState(false)
    const [taskStore, setTaskStore] = useState([])
    const [validated, setValidated] = useState(false);
    const [editValue, setEditValue] = useState("")
    const [newEditedValue, setnewEditedValue] = useState("")
    const [validatedEdit, setvalidatedEdit] = useState(false);
    let [taskdata, setTaskdata] = useState("")

    const handleSubmit = (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            setTaskStore([...taskStore, taskdata]);
            setTaskdata('');
        }
        setValidated(true);
    };

    console.log("taskStore===", taskStore)

    const exportToExcel = () => {

        if (taskStore.length === 0) {
            setErr(true)
            return;
        }
        const workbook = XLSX.utils.book_new();
        const worksheetData = taskStore.map((element) => [element]);
        const worksheet = XLSX.utils.aoa_to_sheet(worksheetData);
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
        XLSX.writeFile(workbook, 'textStore.xlsx');
    };



    const handleChange = (e) => {
        setTaskdata(e.target.value)
        setErr(false)

    }


    // Edit Scenario

    const handleSubmitEdit = (event) => {
        event.preventDefault();
        if (event.currentTarget.checkValidity() === false) {
            event.stopPropagation();
        } else if (changeValueOfEdit) {
            const editedTaskStore = taskStore.map((task, index) => {
                if (index === selectedTaskIndex) {
                    return newEditedValue;
                }
                return task;
            });
            setTaskStore(editedTaskStore);
            setEditValue("");
            setShowDiv(false);
        }
        setValidated(true)
    }


    const handleEdit = (e, idx) => {
        console.log("idx--", idx)
        for (let i = 0; i < taskStore.length; i++) {
            const element = taskStore[i];
            let id = i;
            if (i === idx) {
                setselectedTaskIndex(idx)
                setEditValue(element)   
            }
        }
        setShowDiv(true)
    }
    const handleDelete = (e, idx) => {
        console.log(idx);
        const updatedTaskStore = taskStore.filter((element, index) => index !== idx);
        setTaskStore(updatedTaskStore);
        console.log("updatedTaskStore", updatedTaskStore)
    }


    const handleChangeEdit = (e) => {
        console.log(123)
        setEditValue(prevValue => e.target.value);
        let value = e.target.value
        setnewEditedValue(value)
        setchangeValueOfEdit(true)
        console.log("value===", e.target.value)
        setErr(false)

    }


    return (<>

        <Container>

            <h3 className='text-center my-5 text-light'>Task Management Application</h3>
            <Button type="submit" className='my-5 rounded-0 btn-success' onClick={exportToExcel}><TfiExport/><span className='mx-3'>Export to Excel</span></Button>

            {err ? <>
                {['danger'].map((variant) => (
                    <Alert key={variant} variant={variant} className='alert'>
                        Fill the Field First
                    </Alert>
                ))}
            </> : ""}
            {taskStore.map((element, idx) => {
                console.log("element", idx)
                console.log("changeValueOfEdit--", changeValueOfEdit)
                return (<>
                    <div>
                        <ListGroup>
                            <ListGroup.Item className='sectionTab'>{element}
                                <span>
                                    <TfiMarkerAlt className='editIcons' onClick={(e) => handleEdit(e, idx)} />
                                    <TfiTrash className='deleteIcons' onClick={(e) => handleDelete(e, idx)} />
                                </span>
                            </ListGroup.Item>
                        </ListGroup>
                    </div>
                </>)
            })}

        </Container>

        {showDiv ?

            <Row className='my-5'>
                <Col>
                    <Form noValidate  className="text-center" validated={validatedEdit} onSubmit={handleSubmitEdit}>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="validationCustom01">
                                <Form.Control
                                    required
                                    className='inputBox'
                                    type="text"
                                    placeholder="Edit Task"
                                    value={editValue}
                                    onChange={(e) => handleChangeEdit(e)}
                                />
                            </Form.Group>
                        </Row>
                            <Button type="submit" className='my-5 rounded-0 btn-success'><TfiMarkerAlt className='fs-5'/><span className='mx-3'>Edit Task</span></Button>
                    </Form>
                </Col>
            </Row>

            :
            <Row className='my-5'>
                <Col>
                    <Form noValidate validated={validated} className="text-center" onSubmit={handleSubmit}>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="validationCustom01">
                                <Form.Control
                                    required
                                    className='inputBox'
                                    type="text"
                                    placeholder="task name"
                                    value={taskdata}
                                    onChange={(e) => handleChange(e)}
                                />
                            </Form.Group>
                        </Row>
                            <Button type="submit" className='my-5 rounded-0 btn-success'>+ Add Task</Button>
                    </Form>
                </Col>
            </Row>


        }



    </>)

}

